from unidecode import unidecode
import re

class PollMessage():

    emoji_number = [":zero:", ":one:", ":two:", ":three:", ":four:", ":five:",
                    ":six:", ":seven:", ":eight:", ":nine:", ":keycap_ten:"]

    ATTACHMENT_COLOR_CODE = "#FBE05C"

    MAX_BUTTONS_PER_ROW = 5

    def __init__(self, team, channel, title, options):
        # This is how we identify which button press goes with which message
        self.timestamp = 0
        self.title = title
        # A list containing the text of each option (str) and the people who
        # have called it (list of str).
        # [ {"text": "option1", "users":["userid", "user2id"]},
        #  {"text": "option2", "users": [] ]
        self.options = options
        self.channel_id = channel
        self.team_id = team
        return

    @classmethod
    def build_from_db(cls, team, channel, title, options):
        return cls(team, channel, title, options)

    @classmethod
    def build_from_slash_cmd(cls, team, channel, text: dict):
        formatted_input = cls.format_poll_text(text)
        if formatted_input:
            title = formatted_input.pop(0)
        else:
            title = "If nobody answers, is it still worth asking?"
        options = []
        for option_text in formatted_input:
            options.append({
                "text": option_text,
                "users": []
            })
        return cls(team, channel, title, options)

    @staticmethod
    def format_poll_text(text) -> list:
        normal_fucking_quotes = unidecode(text)
        parsed_list = re.findall(r'"(.+?)"', normal_fucking_quotes)
        return parsed_list

    def button_press_update(self, update_user_id: str, button_number: int):
        # TODO: Maybe support selecting only 1 option in poll here

        users_for_option = self.options[button_number].get("users")
        if update_user_id in users_for_option:
            # User already selected option, remove to support deselecting
            users_for_option.remove(update_user_id)
        else:
            # User hasn't selected this option, add them
            users_for_option.append(update_user_id)
        return

    def build_option_block(self) -> dict:
        block_text = ""
        for option_index, option in enumerate(self.options, 1):
            block_text += f"{self.emoji_number[option_index]} {option['text']}"
            # If someone has clicked this option
            the_users = option['users']
            if the_users:
                # Add count for option
                block_text += f"\t`{len(the_users)}`\n"
                # Add users below option
                for user_index, user_id in enumerate(the_users):
                    if user_index != 0:
                        block_text += ", "
                    block_text += f"<@{user_id}>"
            block_text += "\n\n"

        return {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": block_text
            }
        }

    def build_button_blocks(self, count: int) -> list:
        if count > self.MAX_BUTTONS_PER_ROW:
            return [
                self.build_button_attachment_row(count),
                self.build_button_attachment_row(count, self.MAX_BUTTONS_PER_ROW)
            ]
        else:
            return [self.build_button_attachment_row(count)]

    def build_button_attachment_row(self, count: int, start: int = 0) -> dict:
        # Build the header
        button_attachment = {
                "type": "actions",
                "elements": "list of button dicts",
        }
        # Create a button for each option
        action_buttons = []
        for i in range(count - start):
            action_buttons.append(self.create_button(start+i+1))

        button_attachment['elements'] = action_buttons
        return button_attachment

    def create_button(self, number: int) -> dict:
        return {
            "type": "button",
            "text": {
                "type": "plain_text",
                "text": self.emoji_number[number],
                "emoji": True
            }
        }

    def output_poll_slack_json(self) -> dict:
        title_block = {
            "type": "section",
            "text": {
                "type": "mrkdwn",
                "text": "*" + self.title + "*"
            }
        }
        block_list = [title_block, self.build_option_block(), *self.build_button_blocks(len(self.options))]
        if self.timestamp:
            block_list.append({"type": "divider"})
            block_list.append({
                "type": "context",
                "elements": [{
                    "type": "mrkdwn",
                    "text": f"{self.timestamp}"
                }]
            })
            print(block_list)
        return {
            "text": "",
            "channel": self.channel_id,
            "blocks": block_list
        }
