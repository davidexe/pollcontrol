{
	'type': 'interactive_message',
	'actions': [{
		'name': '3',
		'type': 'button',
		'value': ''
	}],
	'callback_id': 'button_attachment',
	'team': {
		'id': 'TCFC72VM3',
		'domain': 'vbofcourse'
	},
	'channel': {
		'id': 'CJU7C03H7',
		'name': 'testingpoll'
	},
	'user': {
		'id': 'UCGHVG9QW',
		'name': 'davidexe0'
	},
	'action_ts': '1560783924.994774',
	'message_ts': '1560783915.000400',
	'attachment_id': '2',
	'token': 'q0OmLExEk4ZykUNy8GXJoPLI',
	'is_app_unfurl': False,
	'original_message': {
		'type': 'message',
		'subtype': 'bot_message',
		'text': '*is this going to work*',
		'ts': '1560783915.000400',
		'username': 'PollControl',
		'bot_id': 'BJA13D74G',
		'attachments': [{
			'callback_id': 'option_attachment',
			'fallback': "Can't list out the options here",
			'text': ':one: a\n\n:two: b\n\n:three: c\n\n',
			'id': 1
		}, {
			'callback_id': 'button_attachment',
			'fallback': "Buttons aren't supported here ;(",
			'id': 2,
			'color': 'FBE05C',
			'actions': [{
				'id': '1',
				'name': '1',
				'text': ':one:',
				'type': 'button',
				'value': '',
				'style': ''
			}, {
				'id': '2',
				'name': '2',
				'text': ':two:',
				'type': 'button',
				'value': '',
				'style': ''
			}, {
				'id': '3',
				'name': '3',
				'text': ':three:',
				'type': 'button',
				'value': '',
				'style': ''
			}]
		}]
	},
	'response_url': 'https://hooks.slack.com/actions/TCFC72VM3/656026157011/bMbTo4UIHzSRgyY2JwlVS3ui',
	'trigger_id': '656026157203.423415097717.aca51d7f538cdd73639f0dfec5e51c8b'
}