from json import dumps, loads
from message import PollMessage
from requests import post
from unidecode import unidecode
from re import findall

#####INITIALIZE CLOUD FIRESTORE#####
from firebase_admin import initialize_app
from firebase_admin import credentials
from firebase_admin import firestore

cred = credentials.ApplicationDefault()
initialize_app(cred, {
    'projectId': "controlpoll",
})

db = firestore.client()
#####INITIALIZE CLOUD FIRESTORE#####

POST_MESSAGE_URL = "https://slack.com/api/chat.postMessage"
UPDATE_MESSAGE_URL = "https://slack.com/api/chat.update"

OAUTH_ACCESS_TOKEN = "xoxp-423415097717-424607553846-615698995473-fd7e77c5d88b07671d568ba1c6d65d9f"
MESSAGE_HEADERS = {
    'Content-type': 'application/json; charset=utf-8',
    'Authorization': f'Bearer {OAUTH_ACCESS_TOKEN}'
}


def poll_command(request):
    if request.method != "POST":
        return "Only POST requests are accepted", 405

    if request.path == "/command":
        body = request.form
        the_message = PollMessage.build_from_slash_cmd(body.get("team_id"), body.get("channel_id"), body.get("text"))
        send_message_to_slack(the_message)
    elif request.path == "/control":
        handle_indirect_button_press(request.form)
    elif request.path == "/button":
        handle_direct_button_press(loads(request.form.get("payload")))
    elif request.path == "/awake":
        print("Received awake command")

    return '', 200


def handle_indirect_button_press(payload):
    team_id = payload.get("team_id")
    channel_id = payload.get("channel_id")
    control_cmd = payload.get("text")

    control_details = control_cmd.split(" ")

    if len(control_details) > 1:
        username_search = findall(r"@([A-Z0-9]+)\|", control_details[0])
        if username_search:
            user_id = username_search[0]
        else:
            user_id = control_details[0]

    if len(control_details) == 3:
        poll_doc_dict = get_firestore_dict(team_id, channel_id, control_details[2])
    elif len(control_details) == 2:
        poll_doc_dict = get_firestore_dict(team_id, channel_id)
    else:
        # TODO: Handle this error case better for the user
        return '', 400
    
    update_slack_poll_with_new_user(team_id, channel_id, poll_doc_dict, user_id, int(control_details[1]) - 1)


def handle_direct_button_press(payload):
    channel_id = payload["channel"]["id"]
    team_id = payload['team']['id']
    message_ts = payload['message']['ts']
    poll_doc_dict = get_firestore_dict(team_id, channel_id, message_ts)

    update_slack_poll_with_new_user(
        team_id,
        channel_id,
        poll_doc_dict,
        payload["user"]["id"],
        PollMessage.emoji_number.index(payload["actions"][0]["text"]["text"]) - 1
    )


def update_slack_poll_with_new_user(team_id, channel_id, firestore_dict, user_id, button_pressed):
    the_poll_message = PollMessage.build_from_db(team_id, channel_id, firestore_dict.get('title'),
                                                 firestore_dict.get('options'))
    the_poll_message.timestamp = firestore_dict.get('timestamp')
    the_poll_message.button_press_update(user_id, button_pressed)
    send_message_to_slack(the_poll_message)


def get_firestore_dict(team_id, channel_id, message_ts=None) -> dict:
    poll_collection_ref = db.collection(u'teams').document(u'{}'.format(team_id))\
                 .collection(u'channels').document(u'{}'.format(channel_id))\
                 .collection(u'polls')
    if message_ts is None:
        # Get most recent poll if no timestamp given
        poll_query = poll_collection_ref.order_by(u'timestamp', direction=firestore.Query.DESCENDING).limit(1).get()
        print(poll_query)
        for ref in poll_query:
            poll_doc = ref
    else:
        poll_doc = poll_collection_ref.document(u'{}'.format(message_ts)).get()

    return poll_doc.to_dict()


def send_message_to_slack(message_to_send):
    message_json = message_to_send.output_poll_slack_json()
    if message_to_send.timestamp:
        # If the message has a timestamp then we are updating it
        url = UPDATE_MESSAGE_URL
        message_json.update({"ts": message_to_send.timestamp})
    else:
        url = POST_MESSAGE_URL

    resp = post(url, data=dumps(message_json), headers=MESSAGE_HEADERS)
    response = resp.json()

    team_ref = db.collection(u'teams').document(u'{}'.format(message_to_send.team_id))
    channel_ref = team_ref.collection(u'channels').document(u'{}'.format(message_to_send.channel_id))

    if not message_to_send.timestamp:
        # First time posting message, grab its timestamp to update it later
        message_to_send.timestamp = response['ts']
        poll_ref = channel_ref.collection(u'polls').document(u'{}'.format(message_to_send.timestamp))

        poll_ref.set({
            u'title': u'{}'.format(message_to_send.title),
            u'options': message_to_send.options,
            u'timestamp': message_to_send.timestamp
        })
    else:
        # The object already has the timestamp, use that to get the correct poll reference to update with new options
        poll_ref = channel_ref.collection(u'polls').document(u'{}'.format(message_to_send.timestamp))
        # Need to update the users so we keep track of who clicked the button
        poll_ref.set({
            u'options': message_to_send.options
        }, merge=True)
